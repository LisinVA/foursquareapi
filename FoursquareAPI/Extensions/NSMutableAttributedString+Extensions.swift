//
//  NSMutableAttributedString+Extensions.swift
//  FoursquareAPI
//
//  Created by zweqi on 12/06/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import Foundation

extension NSMutableAttributedString {
    
    public convenience init(string: String, fontSize: CGFloat, color: UIColor) {
        let attributes = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: fontSize),
            NSAttributedString.Key.foregroundColor : color
        ]
        self.init(string: string, attributes: attributes)
    }
    
    func appended(string: NSMutableAttributedString) -> NSMutableAttributedString {
        let result = NSMutableAttributedString()
        result.append(self)
        result.append(string)
        return result
    }
}

//extension NSMutableAttributedString {
//
//    static func attributedReviewText(from string: String) -> NSMutableAttributedString {
//        let reviewTitle = NSMutableAttributedString(
//            string: "You wrote a review here: \n",
//            fontSize: 17.0,
//            color: .black
//        )
//        let reviewBody = NSMutableAttributedString(
//            string: string,
//            fontSize: 17.0,
//            color: .gray
//        )
//        return reviewTitle.appended(string: reviewBody)
//    }
//
//    func setBold(_ font: UIFont, forText text: String) {
//        let range: NSRange = self.mutableString.range(of: text)
//        if range.location != NSNotFound {
//            let boldFontAttribute = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: font.pointSize)]
//            self.addAttributes(boldFontAttribute, range: range)
//        }
//    }
//
//    func setColor(_ color: UIColor, forText text: String) {
//        let range: NSRange = self.mutableString.range(of: text)
//        if range.location != NSNotFound {
//            let fontColorAttribute = [NSAttributedString.Key.foregroundColor: color]
//            self.addAttributes(fontColorAttribute, range: range)
//        }
//    }
//}
