//
//  Double+Extensions.swift
//  FoursquareAPI
//
//  Created by zweqi on 12/06/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import Foundation

extension Double {
    
    var stringValue: String {
        return String(self)
    }
}
