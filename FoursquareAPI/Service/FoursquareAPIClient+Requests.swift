//
//  FoursquareAPIClient+Requests.swift
//  FoursquareAPI
//
//  Created by zweqi on 12/06/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import Foundation
import UIKit

extension FoursquareAPIClient {
    
    public func fetchVenues(ll: String,
                            limit: String,
                            intent: SearchIntent,
                            receiptID: String,
                            completion: @escaping (Result<[Venue], FoursquareError>) -> ()) {
        
        let path = "/search/recommendations"
        let parameters = [
            "ll": ll,
            "limit": limit,
            //            "radius": "5000",
            "intent": intent.rawValue,
            "openNow": "1"
        ]
        
        request(path: path, parameters: parameters, receiptID: receiptID) { result in
            DispatchQueue.main.async {
                switch result {
                case let .success(data):
                    do {
                        //                        let jsonFull = try JSONSerialization.jsonObject(with: data, options: [])
                        //                        print(jsonFull)
                        
                        let json = try JSONDecoder().decode(VenuesJSON.self, from: data)
                        let venues = json.asVenues()
                        
                        if venues.count > 0 {
                            completion(.success(venues))
                        } else {
                            let errorMessage = json.response.warning?.text ?? "Not enough results."
                            let error = CustomError(message: errorMessage)
                            completion(.failure(.noResultsError(error)))
                        }
                    } catch {
                        completion(.failure(.jsonParseError(error)))
                    }
                case let .failure(error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    public func fetchVenueDetails(venueId: String,
                                  receiptID: String,
                                  completion: @escaping (Result<VenueDetails, FoursquareError>) -> ()) {
        
        let path = "/venues/\(venueId)"
        
        request(path: path, parameters: [:], receiptID: receiptID) { result in
            DispatchQueue.main.async {
                switch result {
                case let .success(data):
                    do {
                        //                        let jsonFull = try JSONSerialization.jsonObject(with: data, options: [])
                        //                        print(jsonFull)
                        
                        let json = try JSONDecoder().decode(VenueDetailsJSON.self, from: data)
                        
                        completion(.success(VenueDetails(from: json)))
                    } catch {
                        completion(.failure(.jsonParseError(error)))
                    }
                case let .failure(error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    public func fetchVenueImage(from url: URL,
                                receiptID: String,
                                completion: @escaping (Result<UIImage, FoursquareError>) -> ()) {
        
        let request = URLRequest(url: url,
                                 cachePolicy: .returnCacheDataElseLoad,
                                 timeoutInterval: FoursquareAPIClient.timeout)
        
        if let cachedResponse = cache.cachedResponse(for: request),
            let photo = UIImage(data: cachedResponse.data) {
            return completion(.success(photo))
        } else {
            requestReceipts.append(RequestReceipt(receiptID: receiptID, url: url))
            session.dataTask(with: request) { [weak self] data, response, error in
                self?.requestReceipts.removeAll(where: { $0.url == url })
                
                DispatchQueue.main.async {
                    if let error = error {
                        completion(.failure(.connectionError(error)))
                    } else if let data = data, let response = response,
                        let photo = UIImage(data: data) {
                        let cachedResponse = CachedURLResponse(response: response, data: data)
                        self?.cache.storeCachedResponse(cachedResponse, for: request)
                        return completion(.success(photo))
                    } else {
                        fatalError("Unknown error while trying to retrieve venue photo.")
                    }
                }
                }.resume()
        }
    }
}
