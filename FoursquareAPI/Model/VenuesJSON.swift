//
//  VenuesJSON.swift
//  FoursquareAPI
//
//  Created by zweqi on 12/06/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import Foundation

// API: "/search/recommendations"
struct VenuesJSON: Codable {
    let response: Response
    
    struct Response: Codable {
        let warning: Warning?
        let group: Group?
        
        struct Warning: Codable {
            let text: String
        }
        
        struct Group: Codable {
            let results: [Result]?
            
            struct Result: Codable {
                let venue: Venue
                let photo: PhotoURL?
                let snippets: Snippets
                
                struct Venue: Codable {
                    let id: String
                    let name: String
                    let categories: [Category]
                    let location: Location?
                    
                    struct Category: Codable {
                        let primary: Bool
                        let name: String
                        let icon: IconURL?
                    }
                }
                struct Snippets: Codable {
                    let items: [Item]
                    
                    struct Item: Codable {
                        let detail: Detail?
                        
                        struct Detail: Codable {
                            let object: Object
                            
                            struct Object: Codable {
                                let text: String
                            }
                        }
                    }
                }
            }
        }
    }
}

extension VenuesJSON {
    
    func asVenues() -> [Venue] {
        let venues = self.response.group?.results?.map { result -> Venue in
            let foundVenue = result.venue
            let venue = Venue(id: foundVenue.id, name: foundVenue.name)
            
            let category = foundVenue.categories.filter { $0.primary }.first
            venue.category = category?.name
            venue.location = foundVenue.location
            venue.categoryIcon = category?.icon
            venue.bestPhoto = result.photo
            venue.tip = result.snippets.items.first?.detail?.object.text.wrappedInQuotes
            return venue
        }
        return venues ?? []
    }
}

